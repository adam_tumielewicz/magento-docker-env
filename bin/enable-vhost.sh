#!/bin/bash

function init() {
  # Stop on Error
  set -e
  # Stop on undefined variable
  set -u
  # Debug trace
  #set -x
}

function main() {
  init
  loadEnv

  enableVhost "${1}"
}

function enableVhost() {
  local vhostFileName="${1}"

  if [[ ! -f "$(nginxVhostDirPath)/${vhostFileName}" ]]; then
    echo "File : '$(nginxVhostDirPath)/${vhostFileName}' does not exists, exit"
    exit 1
  fi

  ln -sf "$(nginxVhostDirPath)/${vhostFileName}" "$(nginxConfDirPath)/default.conf"

  echo "vHost enabled"
  ls -lah "$(nginxConfDirPath)/default.conf"
}

function nginxConfDirPath() {
  echo "${PWD}/services/nginx/conf.d"
}

function nginxVhostDirPath() {
  echo "${PWD}/services/nginx/vhost"
}

function loadEnv() {
  . .env
}

main "${@}"