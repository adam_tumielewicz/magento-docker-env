#!/bin/bash

function init() {
  # Stop on Error
  set -e
  # Stop on undefined variable
  set -u
  # Debug trace
  #set -x
}

function main() {
  init

  installMagento235 "${@}"
}

function installMagento235() {
  local magentoRootDirPath="${1:-}"

  if [[ "${magentoRootDirPath}" == "" ]]; then
      echo "Specify project root directory path"
      exit 1
  fi

  executePHP "/usr/bin/composer create-project --repository=https://repo.magento.com/ magento/project-community-edition:2.3.5 ${magentoRootDirPath}"
}


function executePHP() {
  local containerName="magento-php"
  local cmd="${1}"

  executeInDockerContainer "${containerName}" "php -d memory_limit=-1 ${cmd}"
}

function executeInDockerContainer() {
  local containerName="${1}"
  shift

  docker exec "${containerName}" bash -c "${*}"
}

function echoGreen() {
  echo -e "Default \e[32m ${*} \e[39mDefault"
}

function echoRed() {
  echo -e "Default \e[91m ${*} \e[39mDefault"
}

main "$@"
